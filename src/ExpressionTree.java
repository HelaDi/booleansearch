import java.util.ArrayList;
import java.util.Stack;

public class ExpressionTree {
	TreeNode root;

	public TreeNode parseTree(String s) {

	String[] sTokens = s.split(" ");
	ArrayList<TreeNode> treeNodes = new ArrayList<TreeNode>();
	TreeNode tempNode = null;


	for (int i = 0; i < sTokens.length; i++) {

		if (sTokens[i].equals("AND")) 
			tempNode = new TreeNode("AND");
		
		if (sTokens[i].equals("OR")) 
			tempNode = new TreeNode("OR");
		
		if (sTokens[i].equals("(")) 
			tempNode = new TreeNode("leftBracket");
		
		if (sTokens[i].equals(")")) 
			tempNode = new TreeNode("rightBracket");
		if(!sTokens[i].equals("AND") 
				&& !sTokens[i].equals("OR")
				&& !sTokens[i].equals("(")
				&& !sTokens[i].equals(")"))
			tempNode = new TreeNode("term", sTokens[i]);
		
		treeNodes.add(tempNode);
	}
	
	Stack<TreeNode> stack = new Stack<TreeNode>();
	TreeNode tempRightNode;
	TreeNode tempOperator;
	TreeNode tempLeftNode;

	if (treeNodes.size() == 3){
		tempLeftNode = treeNodes.get(0);
		tempOperator = treeNodes.get(1);
		tempRightNode = treeNodes.get(2);
		tempOperator.setLeftNode(tempLeftNode);
		tempOperator.setRightNode(tempRightNode);	
		root = tempOperator;	
	}else{
		for (int i = 0; i < treeNodes.size(); i++) {
			stack.push(treeNodes.get(i));
			if (treeNodes.get(i).isRightBracket()) {	
				stack.pop();
				tempRightNode = stack.pop();
				tempOperator = stack.pop();
				tempOperator.setRightNode(tempRightNode);
				tempLeftNode = stack.pop();
				tempOperator.setLeftNode(tempLeftNode);
				stack.pop();
				stack.push(tempOperator);
				root = tempOperator;
			}
			//stack.push(treeNodes.get(i));
		}
	}
	


	return root;
	}
}
