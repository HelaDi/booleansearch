
public class PostingItem {
	private String term;
	private String docID;
	
	PostingItem(){
		this.term = "";
		this.docID = "";
	}
	
	PostingItem(String term, String docID){
		this.term = term;
		this.docID = docID;
	}
	
	void setTerm(String term){
		this.term = term;
	}
	
	String getTerm(){
		return this.term;
	}
	
	void setDocID(String docID){
		this.docID = docID;
	}
	
	String getDocID(){
		return this.docID;
	}
}
