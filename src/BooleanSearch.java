import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BooleanSearch {
	public static void main(String[] args) throws IOException{
		String fileName = args[0];
		InvertedIndex invertedIndex = Parser.parsingFile(fileName);
		System.out.println("Please enter your query.");
		Scanner scanner = new Scanner(System.in);

		String query;
		while(!(query = scanner.nextLine()).equals(":q")){
			ExpressionTree exprTree = new ExpressionTree();
			TreeNode root = exprTree.parseTree(query);
			
			ArrayList<String> outcome = Outcome.queryTree(root, invertedIndex);
			for(int i = 0; i < outcome.size(); i++){
				System.out.print(outcome.get(i));
				System.out.println(" ");
			}
		}
	}
}
