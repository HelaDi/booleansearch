

public class TreeNode {
	private TreeNode leftNode;
	private TreeNode rightNode;
	private String flag;// "AND", "OR", "term", "leftBracket","rightBracket"
	private String termName;

	
	public TreeNode (String flag){
		this.flag = flag;
		this.termName = null;
		leftNode = null;
		rightNode = null;
	}
	
	public TreeNode (String flag, String termName){
		this.flag = flag;
		this.termName = termName;
		leftNode = null;
		rightNode = null;
	}
	
	public String getTermName(){
		return termName;
	}
	
	public String getFlag(){
		return flag;
	}

	public boolean isRightBracket(){
		if (this.flag.equals("rightBracket")){
			return true;
		}
		return false;
	}
	
	public boolean isLeftBracket(){
		if (this.flag.equals("leftBracket")){
			return true;
		}
		return false;
	}
	
	public boolean isTerm(){
		if (this.flag.equals("term")){
			return true;
		}
		return false;
	}
	
	public boolean isAND(){
		if (this.flag.equals("AND")){
			return true;
		}
		return false;
	}
	
	public boolean isOR(){
		if (this.flag.equals("OR")){
			return true;
		}
		return false;
	}
	
	public void setLeftNode(TreeNode leftNode){
		this.leftNode = leftNode;
	}
	
	public TreeNode getLeftNode(){
		return leftNode;
	}
	
	public void setRightNode(TreeNode rightNode){
		this.rightNode = rightNode;
	}
	
	public TreeNode getRightNode(){
		return rightNode;
	}
	
	
	
	
	
	
	
	
	
	
	
}
	