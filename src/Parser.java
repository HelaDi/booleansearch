import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Parser {
	
	public static InvertedIndex parsingFile(String filename) throws IOException {
		FileReader file = new FileReader(filename);
		BufferedReader text = new BufferedReader(file);
		String line;
		String[] tokens;
		PostingTable postingTable = new PostingTable();
		
		while((line = text.readLine()) != null){
			tokens = line.split("\\W+");
			buildPostingTable(tokens, postingTable);
			Arrays.fill(tokens, null); 
		}
		text.close();

		InvertedIndex invertedIndex = new InvertedIndex();
		String tempDocID;
		String tempTerm;
		TermMap tempTermMap;
		for(int i = 0; i < postingTable.tableSize(); i++){
			tempDocID = postingTable.getFromPostingTable(i).getDocID();
			tempTerm = postingTable.getFromPostingTable(i).getTerm();
			
			tempTermMap = invertedIndex.getFromInvertedIndex(tempTerm);
			if(tempTermMap == null){
				tempTermMap = new TermMap(tempTerm);
				invertedIndex.setIntoInvertedIndex(tempTerm, tempTermMap);				
			}
			tempTermMap.setIntoPostingList(tempDocID);
		}

		return invertedIndex;
	}
	
	public static void buildPostingTable(String[] tokens, PostingTable postingTalbe){
		if(tokens.length == 0)
			return;
		String docID = tokens[0];
		PostingItem tempPostingItem = null;
		for(int i = 1; i < tokens.length; i++){
			tempPostingItem = new PostingItem(tokens[i], docID);
			postingTalbe.setIntoPostingTable(tempPostingItem);
		}
	}
}
