import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Outcome {

		public static ArrayList<String> queryTree(TreeNode root, InvertedIndex invertedIndex) throws IOException{
			ArrayList<String> tempLeftPostList = null;
			
			tempLeftPostList = postOrderTrav(root, invertedIndex);			
	
			return tempLeftPostList;
			
		} 
		
		private static ArrayList<String> postOrderTrav(TreeNode currRoot, InvertedIndex invertedIndex){
			ArrayList<String> result = new ArrayList<>();
			if(currRoot == null){
				return result;
			}
			
			ArrayList<String> tempLeftPostList;
			ArrayList<String> tempRightPostList;
			tempLeftPostList = postOrderTrav(currRoot.getLeftNode(), invertedIndex);
			tempRightPostList = postOrderTrav(currRoot.getRightNode(), invertedIndex);
			TermMap tempTermMap;
			
			if (currRoot.isTerm()){	
				tempTermMap = invertedIndex.getFromInvertedIndex(currRoot.getTermName());
				result = tempTermMap.getPostListFromTermMap();
				return result;
			}

			if(currRoot.isAND() && !tempLeftPostList.isEmpty() && !tempRightPostList.isEmpty()){
				tempLeftPostList.retainAll(tempRightPostList);
				Collections.sort(tempLeftPostList);
				return tempLeftPostList;
			}
			
			if(currRoot.isOR() && !tempLeftPostList.isEmpty() && !tempRightPostList.isEmpty()){
				tempLeftPostList.removeAll(tempRightPostList);
				tempLeftPostList.addAll(tempRightPostList);
				Collections.sort(tempLeftPostList);
				return tempLeftPostList;
			}
			
		return tempRightPostList;
			
		}

}
