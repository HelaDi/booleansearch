import java.io.IOException;
import java.util.ArrayList;
//import java.util.LinkedHashSet;

public class Test {
	public static void main(String[] args) throws IOException {

		String fileName = "/Users/dihela/Desktop/Java/BooleanSearch/src/123.txt";
		//Parser.parsingFile(fileName);
		InvertedIndex invertedIndex = Parser.parsingFile(fileName);

		String query = "( ( schizophrenia AND drug ) AND new )";
		ExpressionTree expreTree = new ExpressionTree();
		TreeNode root = expreTree.parseTree(query);
		ArrayList<String> outcome = Outcome.queryTree(root, invertedIndex);
		
		//LinkedHashSet<String> tempOutcome = new LinkedHashSet<String>();
		//lhs.addAll(outcome);
		//outcome.clear();
		//outcome.addAll(tempOutcome);
		
		for (int i = 0; i < outcome.size(); i++) {
			System.out.println(outcome.get(i));
		}
	}
}
