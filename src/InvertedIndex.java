import java.util.HashMap;

public class InvertedIndex {
	private HashMap<String,TermMap> hashmap = new HashMap<>();
	
	public void setIntoInvertedIndex(String term, TermMap termMap){
		hashmap.put(term, termMap);		
	}
	
	public TermMap getFromInvertedIndex(String term){
		return hashmap.get(term);
	}
	
}
