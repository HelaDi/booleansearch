import java.util.ArrayList;

public class TermMap {
	private String term;
	private ArrayList<String> postingList = new ArrayList<>();
	
	public TermMap(){
		this.term = "";
	}
	
	public TermMap(String term){
		this.term = term;
	}
	
	public void setTerm(String term){
		this.term = term;
	}
	
	public String getTerm(){
		return this.term;
	}
	
	public int postingListSize(){
		return postingList.size();
	}
	
	public void setIntoPostingList(String posting){
		postingList.add(posting);
	}
	
	public String getFromPostingList(int index){
		return postingList.get(index);		
	}
	
	public ArrayList<String> getPostListFromTermMap(){
		return postingList;
	}

	
}