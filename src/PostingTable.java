import java.util.ArrayList;

public class PostingTable {
	private ArrayList<PostingItem> postingItems = new ArrayList<PostingItem>();
	
	public PostingTable(){
		//postingItems = new ArrayList<PostingItem>();
	}
	public void setIntoPostingTable(PostingItem postingItem){
		postingItems.add(postingItem);
	}
	
	public PostingItem getFromPostingTable(int index){
		return postingItems.get(index);
	}
	
	public int tableSize(){
		return postingItems.size();
	}
}
